var express = require('express');
var moment = require('moment');
var router = express.Router();
var models  = require('../models');

router.get('/', function(req, res, next) {
  models.Movie.findAll({}).then( movies =>
    res.render('movies/index', { movies: movies, moment: moment })
  );
});


router.get('/new', function(req, res, next) {
  res.render('movies/new');
});



router.post('/', function(req, res, next) {
  models.Movie.create(req.body).then(movie =>
    res.redirect('/movies')
  )
});



router.get("/:id", function (req,res,next) {
  models.Movie
  .findByPk(req.params.id, {include: models.Review})
  .then(movie =>
    // LAZY
    // movie.getReviews().then(reviews => 
    //   res.render('movies/show', { movie: movie, reviews: reviews, moment: moment })
    // )

    // EAGER
    res.render('movies/show', {movie: movie, moment: moment})
  );
})


router.post("/:id/reviews", function(req,res,next) {
  models.Movie.findByPk(req.params.id).then(movie => {
    models.Review.create({body: req.body.body, movie_id: movie.id}). then(review =>
      res.redirect('/movies/' + movie.id)
    );
  });
});


router.get("/:id/edit", function (req,res,next) {
  // "req.params.id" to access the movie id
  // ... add your code here
})


module.exports = router;
