'use strict';
module.exports = (sequelize, DataTypes) => {
  const Movie = sequelize.define('Movie', {
    title: DataTypes.STRING,
    description: DataTypes.STRING,
    rating: DataTypes.STRING,
    release_date: DataTypes.DATE
  }, {});
  Movie.associate = function(models) {
    Movie.hasMany(models.Review, { 
      foreignKey: 'movie_id'
    });
  };
  return Movie;
};